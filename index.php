<?php
require "vendor/autoload.php";
$app = new \Slim\App();
$app->get('/customers/{number}', function($request, $response,$args){
    $json = '{"1":"john", "2":"jack"}';
    $array = (json_decode($json, true));
    if(array_key_exists($args['number'], $array)){
        echo $array[$args['number']];
    }
    else{
        echo "This user not found";
    }

});
$app->run();